import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { PermissionGuard } from './helpers/permission.guard';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Mini-Project';
  user:any

  constructor(
    private router: Router,
    private permissionGuard: PermissionGuard,
    private authService: AuthService
  ){}

  currentpage: string =''
  
  ngOnInit(): void {
    this.router.events.subscribe((event:any)=>{
      if(event instanceof NavigationEnd){
        this.currentpage=(<NavigationEnd>event).url
      }
    })
  }

  name(){
    const getAccessToken = String(localStorage.getItem('access_token'))

    this.authService.getUser(getAccessToken).subscribe((data: any) => {
      this.user = data.data.name
      console.log(this.user);
    })
  }

  logout(){
    this.permissionGuard.logout()
    if(!localStorage.getItem('access_token')){
      this.router.navigate(['login'])
    }
  }
}
