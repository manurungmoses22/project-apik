import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user:any

  constructor(
    private authService : AuthService,
  ) { }

  ngOnInit(): void {
    this.viewProfile()
  }

  viewProfile(){
    const getAccessToken = String(localStorage.getItem('access_token'))

    this.authService.getUser(getAccessToken).subscribe((data: any) => {
      this.user = data.data
      console.log(this.user);
      
    })
  }

}
