import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private root: ActivatedRoute,
    private authservice: AuthService,
    private router: Router,
  ){}

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required
    ])
  })

  urlAPI = 'http://localhost:3001/user'

  get email() {
    return this.form.get('email')
  }
  get password() {
    return this.form.get('password')
  }

  ngOnInit(): void {
    
  }

  formSubmit(){
    const { email, password } = this.form.value

    this.authservice.login({
      email, password
    }).subscribe((data: any) => {
      if(data){
        this.router.navigate([''])
        localStorage.setItem('access_token',data.token)
      }
    })

    // const val = this.form.value
    // if(val.email && val.password){
    //   this.authservice.login(val.email, val.password)
    //   .subscribe(
    //     ()=>{
    //       console.log(val, "<<< User is logged in");
    //       this.router.navigateByUrl('/')
    //     }
    //   )
    // }
  }

}
