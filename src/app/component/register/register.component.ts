import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private root: ActivatedRoute,
    private authService : AuthService,
    private router: Router,
  ){}

  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = ''
  
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required
    ])
  })

  get name() {
    return this.form.get('name')
  }
  get email() {
    return this.form.get('email')
  }
  get password() {
    return this.form.get('password')
  }

  ngOnInit(): void {
    
  }

  formSubmit(){
    const {name, email, password } = this.form.value

    this.authService.login({
      email, password
    }).subscribe((data: any) => {
      if(data){
        this.router.navigate(['/login'])
        localStorage.setItem('access_token',data.token)
      }
    })
  }

  //   const userRegister = {
  //     name: this.form.value.name??'',
  //     email: this.form.value.email??'',
  //     password: this.form.value.password??''
  //   }
  //   this.authService.register(userRegister).subscribe({
  //     next: data => {
  //       console.log(data);
  //       this.isSuccessful = true
  //       this.isSignUpFailed = false
  //     },
  //     error: err => {
  //       this.errorMessage = err.error.message
  //       this.isSignUpFailed = true
  //     }
  //   })
  // }
}
