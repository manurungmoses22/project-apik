import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { findIndex, timer } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  data: any
  Infinance: any[] = []
  Outfinance: any[] = []
  total: any
  amountIndex: number = 0
  idIndex: number = 0
  user: any
  PostFinance: any = {
    type: '',
    title: '',
    amount: 0,
    desc: ''
  }
  constructor(
    private root: ActivatedRoute,
    private userService: UserService,
    private authService: AuthService
  ) { }

  types: string[] = [
    'income',
    'outcome',
  ]

  form = new FormGroup({
    type: new FormControl('', Validators.required),
    title: new FormControl('', [Validators.required,]),
    amount: new FormControl(0, [Validators.required, Validators.min(1)]),
    desc: new FormControl(''),
  })

  get type() {
    return this.form.get('type')
  }
  get title() {
    return this.form.get('title')
  }
  get amount() {
    return this.form.get('amount')
  }
  get desc() {
    return this.form.get('desc')
  }

  ngOnInit(): void {
    this.viewFinance()
  }

  viewFinance() {
    const getAccessToken = String(localStorage.getItem('access_token'))

    this.authService.getUser(getAccessToken).subscribe((data: any) => {
      this.user = data.data.name
      console.log(this.user);
      
    })
    
    this.userService.getWallet(getAccessToken).subscribe((data: any) => {
      this.data = data.data.amount
      console.log(data);
      
    })

    this.userService.getFinance(getAccessToken).subscribe((data: any) => {
      this.Infinance = []
      this.Outfinance = []

      for (let i = 0; i < data.data.length; i++) {
        if (data.data[i].type === "income") {
          this.Infinance[i] = data.data[i]

        }
        else {
          this.Outfinance[i] = data.data[i]
        }
      }
    })
  }

  findIndex(needamount: number) {
    this.amountIndex = this.Infinance.findIndex((x: any) => { return x?.amount === needamount })
  }
  findIndexOut(needamount: number) {
    this.amountIndex = this.Outfinance.findIndex((x: any) => { return x?.amount === needamount })
  }

  formSubmit(): void {
    const getAccessToken = String(localStorage.getItem('access_token'))

    const type = this.form.value.type ?? ''
    const title = this.form.value.title ?? ''
    const amount = this.form.value.amount ?? 0
    const desc = this.form.value.desc ?? ''
    this.userService.postFinance(type, title, amount, desc, getAccessToken).subscribe((data: any) => {
      console.log(data);
      this.viewFinance()
      this.total = data
      console.log(this.total, '<<<<Mytotal')
    })

    const PostFinance = {
      type: this.form.value.type ?? '',
      title: this.form.value.title ?? '',
      amount: this.form.value.amount ?? 0,
      desc: this.form.value.desc ?? ''
    }

    if (PostFinance.type == 'income') {
      this.Infinance.push(PostFinance)
      this.findIndex(PostFinance.amount)
      console.log(this.Infinance[this.amountIndex].amount);
      const income = this.Infinance[this.amountIndex]
      this.userService.topUp(income, getAccessToken).subscribe((data: any) => {
      this.viewFinance()
      console.log(data);
    })
    }
    else {
      this.Outfinance.push(PostFinance)
      this.findIndexOut(PostFinance.amount)
      console.log(this.Outfinance[this.amountIndex].amount);
      const outcome = this.Outfinance[this.amountIndex]
      this.userService.topUp(outcome, getAccessToken).subscribe((data: any) => {
      this.viewFinance()
      console.log(data);
    })
    }
  }

  deleteFinance(IdFinance: any) {
    const getAccessToken = String(localStorage.getItem('access_token'))
    console.log(IdFinance);

    this.userService.deleteFinance(IdFinance._id, getAccessToken).subscribe((data: any) => {
      this.viewFinance()
      console.log(data, "<<<<<")
    })

    const PostFinance ={
      type: this.form.value.type??'',
      title: this.form.value.title??'',
      amount: this.form.value.amount??0,
      desc: this.form.value.desc??''
    }
  
    if(IdFinance.type == 'income'){
      
      this.Infinance.push(PostFinance)
      this.findIndex(PostFinance.amount)
      console.log(this.Infinance[this.amountIndex].amount);

      const outcome = IdFinance
      
      console.log(outcome, ',< outcome');
      this.userService.EdittopUp(outcome, getAccessToken).subscribe((data:any)=>{
        this.viewFinance()
        console.log(data);
      })
    }
    else{
      this.Outfinance.push(PostFinance)
      this.findIndexOut(PostFinance.amount)
      console.log(this.amountIndex);
      
      console.log(this.Outfinance[this.amountIndex].amount);
      const income = IdFinance
      console.log(income,'ajhfhfewhfo');
      
      this.userService.EdittopUp(income, getAccessToken).subscribe((data:any)=>{
        this.viewFinance()
      })
    }
  }

  findIndexEditIn(needid: string) {
    this.idIndex = this.Infinance.findIndex((x: any) => { return x?._id === needid })
  }

  editFinanceIn(id:string){
    this.findIndexEditIn(id)
    console.log(this.findIndexEditIn(id));
    this.form.patchValue(this.Infinance[this.idIndex])
  }

  updateFinanceIncome(idFinance:string){
    const type = this.form.value.type ?? ''
    const title = this.form.value.title ?? ''
    const amount = this.form.value.amount ?? 0
    console.log(amount);
    
    const desc = this.form.value.desc ?? ''
    const getAccessToken = String(localStorage.getItem('access_token'))
    this.findIndexEditIn(idFinance)
    const updateAmount = this.Infinance[this.idIndex].amount-amount
    console.log(updateAmount);
    
    this.userService.editFinance(idFinance, type, title, amount, desc, getAccessToken).subscribe((data:any)=>{
      console.log(data);
      this.viewFinance()
    })

    const PostFinance = {
      type: this.form.value.type ?? '',
      title: this.form.value.title ?? '',
      amount: updateAmount,
      desc: this.form.value.desc ?? ''
    }

    if(updateAmount > 0){
      if (PostFinance.type == 'income') {
        this.Infinance.push(PostFinance)
        this.findIndex(PostFinance.amount)
        console.log(this.Infinance[this.amountIndex].amount);
        const income = this.Infinance[this.amountIndex]
        console.log(income);
        
        this.userService.EdittopUp(income, getAccessToken).subscribe((data: any) => {
        this.viewFinance()
        console.log(data);
      })
      }
    }
    else{
      if (PostFinance.type == 'income') {
        this.Infinance.push(PostFinance)
        this.findIndex(PostFinance.amount)
        console.log(this.Infinance[this.amountIndex].amount);
        const abs = Math.abs(this.Infinance[this.amountIndex].amount)
        // console.log(Math.abs(this.Infinance[this.amountIndex].amount));
        this.Infinance[this.amountIndex].amount = abs
        console.log( this.Infinance[this.amountIndex].amount);
        
        const income = this.Infinance[this.amountIndex]
        console.log(income)
        this.userService.topUp(income, getAccessToken).subscribe((data: any) => {
        this.viewFinance()
        console.log(data);
      })
      }
    }
  }


  findIndexEditOut(needid: string) {
    this.idIndex = this.Outfinance.findIndex((x: any) => { return x?._id === needid })
  }
  editFinanceOut(id:string){
    this.findIndexEditOut(id)
    console.log(this.findIndexEditOut(id));
    this.form.patchValue(this.Outfinance[this.idIndex])
  }

  updateFinanceOutcome(idFinance:string){
    const type = this.form.value.type ?? ''
    const title = this.form.value.title ?? ''
    const amount = this.form.value.amount ?? 0
    console.log(amount);
    const desc = this.form.value.desc ?? ''
    const getAccessToken = String(localStorage.getItem('access_token'))
    this.findIndexEditOut(idFinance)
    const updateAmount = amount - this.Outfinance[this.idIndex].amount
    console.log(updateAmount);
    
    this.userService.editFinance(idFinance, type, title, amount, desc, getAccessToken).subscribe((data:any)=>{
      console.log(data);
      this.viewFinance()
    })

    const PostFinance = {
      type: this.form.value.type ?? '',
      title: this.form.value.title ?? '',
      amount: updateAmount,
      desc: this.form.value.desc ?? ''
    }

    if(updateAmount > 0){
      if (PostFinance.type == 'outcome') {
        this.Outfinance.push(PostFinance)
        this.findIndexOut(PostFinance.amount)
        console.log(this.Outfinance[this.amountIndex].amount);
        const outcome = this.Outfinance[this.amountIndex]
        console.log(outcome);
        
        this.userService.topUp(outcome, getAccessToken).subscribe((data: any) => {
        this.viewFinance()
        console.log(data);
      })
      }
    }
    else{
      if (PostFinance.type == 'outcome') {
        this.Outfinance.push(PostFinance)
        this.findIndexOut(PostFinance.amount)
        console.log(this.amountIndex);
        // console.log(Math.abs(this.Outfinance[this.amountIndex].amount));
        const abs = Math.abs(this.Outfinance[this.amountIndex].amount)
        this.Outfinance[this.amountIndex].amount = abs
        console.log( this.Outfinance[this.amountIndex].amount);
        
        const outcome = this.Outfinance[this.amountIndex]
        console.log(outcome)
        this.userService.EdittopUp(outcome, getAccessToken).subscribe((data: any) => {
        this.viewFinance()
        console.log(data);
      })
      }
    }
  }

}
