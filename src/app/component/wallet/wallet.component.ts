import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {
  myammount: any
  user: any
  idIndex: number = 0
  letId: any

  urlApi = 'http://localhost:3001/wallet/topup'
  constructor(
    private root: ActivatedRoute,
    private userService: UserService,
    private authService: AuthService
  ) { }

  getAccessToken: string = ''

  wallet: any[] = [{
    _id: '',
    amount: 0
  }]


  form = new FormGroup({
    amount: new FormControl(0, [Validators.required]),
  })
  get amount() {
    return this.form.get('amount')
  }

  ngOnInit(): void {
    const getAccessToken = String(localStorage.getItem('access_token'))

    this.userService.getWallet(getAccessToken).subscribe((data: any) => {
      this.myammount = data.data.amount
    })
    

    this.authService.getUser(getAccessToken).subscribe((data: any) => {
      this.user = data.data.name
      console.log(this.user);
      
    })
  }

  // findIndex(needId: string) {
  //   this.idIndex = this.myUser.findIndex((x: any) => { return x.id === needId })
  //   console.log(this.idIndex)
  // }

  formSubmit() {
    // const getAccessToken = String(localStorage.getItem('access_token'))
    // const { amount } = this.form.value
    // this.userService.topUp(amount, getAccessToken).subscribe((data: any) => {
    // })
  }
  
}
