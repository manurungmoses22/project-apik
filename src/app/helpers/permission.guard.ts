import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

  constructor(
    private router: Router,
  ){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const getAccessToken = localStorage.getItem('access_token');
      const ActivePath = state.url
    if((ActivePath === ('/') || ActivePath === ('/wallet') || ActivePath === ('/profile'))&&(!getAccessToken)){
        this.router.navigate(['/login'])
      }
      return true
      // switch (ActivePath){
      //   case "/register":
      //   case "/login":
      //     getAccessToken ? this.router.navigate(['']) : true
      //     return true

      //   default:
      //     getAccessToken ? true : this.router.navigate(['login'])
      //     return true
      // }
  }

  logout(): void {
    window.localStorage.clear()
  }
  
}
