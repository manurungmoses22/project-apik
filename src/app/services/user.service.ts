import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Transaction, Wallet } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  urlApi = 'http://localhost:3001'

  getWallet(token: string){
    return this.http.get(`${this.urlApi}/wallet`,{
      headers: {'Authorization':`Bearer ${token}`}
    })
  }

  getFinance(token:string){
    return this.http.get(`${this.urlApi}/finance`,{
      headers: {'Authorization':`Bearer ${token}`}
    })
  }

  postFinance(type:string, title:string, amount:number, desc:string, token:string){
    return this.http.post(`${this.urlApi}/finance`, {type,title,amount,desc}, {
      headers: {'Authorization':`Bearer ${token}`}
    })
  }

  deleteFinance(idFinance:string, token:string){
    return this.http.delete(`${this.urlApi}/finance/${idFinance}`,{
      headers: {'Authorization' :`Bearer ${token}`} 
    })
  }
  
  editFinance(_id:string, type:string, title:string, amount:number, desc:string, token:string){
    return this.http.put(`${this.urlApi}/finance/${_id}`, {type,title,amount,desc}, {
      headers: {'Authorization':`Bearer ${token}`}
    })
  }

  topUp(transaction:any, token:string){
    console.log(transaction);
    
    if(transaction.type=='income'){
      const obj = {
        income: transaction.amount
      } 
      return this.http.put(`${this.urlApi}/wallet`, obj ,{
        headers: {'Authorization':`Bearer ${token}`}
      })
    }
    else{
      const obj = {
        outcome: transaction.amount
      } 
      return this.http.put(`${this.urlApi}/wallet`, obj ,{
        headers: {'Authorization':`Bearer ${token}`}
      })
    }
    
  }
  EdittopUp(transaction:any, token:string){
    console.log(transaction);
    
    if(transaction.type=='outcome'){
      const obj = {
        income: transaction.amount
      } 
      return this.http.put(`${this.urlApi}/wallet`, obj ,{
        headers: {'Authorization':`Bearer ${token}`}
      })
    }
    else{
      const obj = {
        outcome: transaction.amount
      } 
      return this.http.put(`${this.urlApi}/wallet`, obj ,{
        headers: {'Authorization':`Bearer ${token}`}
      })
    }
    
  }
}
