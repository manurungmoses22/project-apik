import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  urlAPI = 'http://localhost:3001/user'

  login(data: any){
    return this.http.post(`${this.urlAPI}/login`,data)
  }
  
  register(data: any){
    return this.http.post(`${this.urlAPI}/register`,data)
  }

  getUser(token: string){
    return this.http.get(`${this.urlAPI}/profile`,{
      headers:{'Authorization':`Bearer ${token}`}
    })
  }
}
