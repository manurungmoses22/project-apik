export interface Wallet {
    _id: string;
    amount: any;
}

export interface Transaction{
    _id: string
    type: string;
    title: string;
    amount: number;
    desc: string;
}