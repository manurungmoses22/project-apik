import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './component/profile/profile.component';
import { TransactionComponent } from './component/transaction/transaction.component';
import { WalletComponent } from './component/wallet/wallet.component';
import { PermissionGuard } from './helpers/permission.guard';

const routes: Routes = [
  {path: '', component: TransactionComponent, canActivate:[PermissionGuard]},
  {path: 'wallet', component: WalletComponent, canActivate:[PermissionGuard]},
  {path: 'profile', component: ProfileComponent, canActivate:[PermissionGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
