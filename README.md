# Mini Project Aplikasi Manajemen Keuangan
1. application system / flow
    a. Setiap user harus memiliki akun yang dapat didaftarkan pada laman registrasi
    b. Setelah terdaftar, user dapat login menggunakan email dan password
    c. Setelah masuk, user dapat mendaftarkan transaksi yang telah terjadi, baik itu income maupun outcome
    d. User dapat melakuakn perubahan data transaksi dengan mengklik button edit dan mengedit data transaksi
    e. user dapat melakukan penghapusan data transaksi dengan mengklik button delete
2. feature
    a. create user
    b. create, edit, delete transaksi
    c. profile untuk melihat data user
    d. wallet untuk melihat keterangan wallet
3. dokumentasi
    a. Login 
    ![My image](src/assets/login.png)
    b. Register
    ![My image](src/assets/register.png)
    c. Transaksi
    ![My image](src/assets/transaksi.png)
    d. Wallet
    ![My image](src/assets/wallet.png)
    e. Profile
    ![My image](src/assets/profile.png)
    f. Transaksi Baru
    ![My image](src/assets/create_transaksi.png)
    g. Edit Transaksi
    ![My image](src/assets/edit_transaksi.png)


# MiniProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
